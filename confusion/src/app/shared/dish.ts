import { Comment } from './Comment';

export class Dish {
    id: string;
    name: string;
    image: string;
    category: string;
    featured: boolean;
    price: string;
    label: string;
    description: string;
    comments: Comment[];
 }
