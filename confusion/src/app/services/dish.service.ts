import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor() { }
// code using promises
//   getDishes(): Promise<Dish[]> {
//     return new Promise(resolve => {
//       // simulate server latency with 2 sec delay
//       setTimeout(() => resolve(DISHES), 3000);
//          }
//       );
//   }

//   getDish(id: string): Promise<Dish> {
//     return new Promise(resolve => {
//       // simulate server latency with 2 sec delay
//       setTimeout(() => resolve(DISHES.filter((dish) => dish.featured)[0]), 3000);
//          }
//       );
//   }

//   getFeaturedDish():  Promise<Dish> {
//     return  new Promise(resolve => {
//       // simulate server latency with 2 sec delay
//       setTimeout(() => resolve(DISHES.filter((dish) => dish.featured)[0]), 3000);
//          }
//       );
// }



// code using Observables and returning promises
// getDishes(): Promise<Dish[]> {
//   return of(DISHES).pipe(delay(2000)).toPromise();
// }

// getDish(id: number): Promise<Dish> {
//   return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(2000)).toPromise();
// }

// getFeaturedDish(): Promise<Dish> {
//   return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(2000)).toPromise();
// }

getDishes(): Observable<Dish[]> {
  return of(DISHES).pipe(delay(2000));
}

getDish(id: string): Observable<Dish> {
  return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(2000));
}

getFeaturedDish(): Observable<Dish> {
  return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(2000));
}

getDishIds(): Observable<string[] | any> {
  return of(DISHES.map(dish => dish.id ));
}

}
